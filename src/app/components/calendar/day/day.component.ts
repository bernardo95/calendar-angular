import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DayModel, EventModel } from '../../../models/day.models';
import { DataService } from 'src/app/services/data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent implements OnInit {
  @Input() day: DayModel;
  @Output() outputSelectedDay = new EventEmitter<DayModel>();

  eventsObservable$: Observable<EventModel>;

  constructor(public dataService: DataService) {}

  ngOnInit(): void {
    this.eventsObservable$ = this.dataService.getObservableEvents$();
    this.subscribeEventsObservable(this.day);
  };

  // Selects the correspondent day
  selectedDay(day?: DayModel) {
    this.dataService.selectedDay = day;
    this.dataService.getEventsSelectedDay(day);

    // Send data to Modal List
    this.outputSelectedDay.emit(day);
  };
  
  // Sort events by start time remender. 
  sortEventsByStartTime(day: DayModel) {
    day.events = day.events.sort((current, next) => {
      if (current.startTime > next.startTime) {
        return 1;
      }
      if (current.startTime < next.startTime) {
        return -1;
      }
      // Current is equal to next
      return 0;
    });
    return day;
  };

  // Init subscripcion observable
  subscribeEventsObservable(day: DayModel) {
    this.dataService.getEventsSelectedDay(day);
    this.eventsObservable$.subscribe(event => {
      if (day.id == event.id.split('-')[0]) {
        this.day.events.push(event);
      }
    });
    this.day = this.sortEventsByStartTime(this.day);
  };

}


