import { Component, OnInit, ElementRef, ViewChild } from "@angular/core";
import { UtilsService } from '../../services/utils.service';

// Models
import { DayModel, EventModel } from 'src/app/models/day.models';

// Services
import { DataService } from '../../services/data.service';
import { ModalListComponent } from './modal-list/modal-list.component';
import { DayComponent } from './day/day.component';

@Component({
    selector: "app-calendar",
    templateUrl: "./calendar.component.html",
    styleUrls: ["./calendar.component.css"],
})
export class CalendarComponent implements OnInit {
    // DOM elements
    calendar: HTMLElement;
    weekdays: HTMLElement;
    btnRigth: HTMLButtonElement;
    btnLeft: HTMLButtonElement;

    // Titles
    title: String;
    currentDate: String;

    // Dates
    year: number;
    month: number;
    startDay: String;
    positionInitDay: number;
    listDaysName: Array<String>;
    lastDayMonthName: String;

    // Controls temp calendar data-flow
    daysMonth: Array<DayModel> = [];

    // Storage
    localStorageArray = [];

    // Child Modal List Component
    @ViewChild(ModalListComponent) modalListComponent: ModalListComponent;
    // Child Day Component
    // @ViewChild(DayComponent) dayComponent: DayComponent;



    constructor(private elRef: ElementRef, public utilsService: UtilsService, public dataService:DataService) { }

    ngOnInit(): void {
        this.initData();
        this.initTitle();
    };

    ngAfterViewInit(): void {
        //Called after ngAfterContentInit when the component's view has been initialized. Applies to components only.
        //Add 'implements AfterViewInit' to the class.
        this.initAfterView();
    };

    





    // ########      ######   #######  ##    ## ######## ########   #######  ##       ##       ######## ########  
    // ##           ##    ## ##     ## ###   ##    ##    ##     ## ##     ## ##       ##       ##       ##     ## 
    // ##           ##       ##     ## ####  ##    ##    ##     ## ##     ## ##       ##       ##       ##     ## 
    // ######       ##       ##     ## ## ## ##    ##    ########  ##     ## ##       ##       ######   ########  
    // ##           ##       ##     ## ##  ####    ##    ##   ##   ##     ## ##       ##       ##       ##   ##   
    // ##       ### ##    ## ##     ## ##   ###    ##    ##    ##  ##     ## ##       ##       ##       ##    ##  
    // ##       ###  ######   #######  ##    ##    ##    ##     ##  #######  ######## ######## ######## ##     ## 
    // Initialize DOM title
    initTitle() {
        this.title = new Date(this.year, this.month, 1).toDateString();
        this.currentDate = this.title.split(" ")[1] + " " + this.title.split(" ")[3];
    };

    // Initialize data
    initData() {
        this.year = new Date().getFullYear();
        this.month = new Date().getMonth();
        this.listDaysName = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
        ];
    };

    // Initialize/Generate DOM content
    initAfterView() {
        this.lastDayMonthName = this.utilsService.getLastDayMonthName(this.year, this.month);
        this.startDay = this.utilsService.initialDay(this.year, this.month);
        this.positionInitDay = this.utilsService.positionDayinit(this.startDay, this.listDaysName);
        this.calendar = this.elRef.nativeElement.querySelector("#app-calendar");
        this.weekdays = this.elRef.nativeElement.querySelector("#weekdays");
        this.btnRigth = this.elRef.nativeElement.querySelector(".arrowbtnright");
        this.btnLeft = this.elRef.nativeElement.querySelector(".arrowbtnleft");
        this.getDaysBeforeMonth(this.year, this.month);
        // +1 since we need to check the current month in Date(year, month, 0)
        this.getDaysMonth(this.year, this.month + 1);
        this.getDayAfterMonth(this.year, this.month);
    };

    // Generates current month days
    getDaysMonth(year, month) {
        for (let day = 1; day <= this.utilsService.getAmountDaysMonth(year, month); day++) {
            // -1 since we need the current month
            this.pushDaysMonth(year, month, day, -1, false);
        }
    };

    // Generates days before month
    getDaysBeforeMonth(year, month) {
        for (
            let day = this.utilsService.getAmountDaysMonth(year, this.utilsService.getMonthBefore(year, month));
            day >
            this.utilsService.getAmountDaysMonth(year, this.utilsService.getMonthBefore(year, month)) -
            this.positionInitDay +
            1;
            day--
        ) {
            // -1 means previus month
            this.pushDaysMonth(year, month, day, -1, true);
        }
        this.daysMonth.reverse();
    };

    // Generates days before month
    getDayAfterMonth(year, month) {
        for (
            let day = 1;
            day <= 6 - this.utilsService.positionDayFinish(this.lastDayMonthName, this.listDaysName);
            day++
        ) {
            this.pushDaysMonth(year, month, day, 1, true);
        }
    };

    // Adds day model to daysMonth
    // Dash is the offset of the month
    pushDaysMonth(year: number, month: number, day: number, dash: number, dayNotCurrentMonth: boolean) {
        let dayTemp: DayModel;
        let style: any;

        let dayName = this.utilsService.getDayName(this.utilsService.getDate(year, month + dash).setDate(day));
        const isWeekend = this.utilsService.isWeekend(dayName);

        style = {
            "isWeekend": isWeekend,
            "dayNotCurrentMonth": dayNotCurrentMonth,
            "selectedDay": false,
        };

        dayTemp = new DayModel();
        dayTemp.id = `${year}${month}${day}`;

        this.setLocalStorageData(dayTemp);

        dayTemp.day = day;
        dayTemp.month = month;
        dayTemp.year = year;
        dayTemp.styles = style;    
        // dayTemp.events = findDay['events'];
        this.daysMonth.push(dayTemp);
    };

    // Sets dayTemp in localstorage
    setLocalStorageData(dayTemp: DayModel) {
        let localStore = JSON.parse(localStorage.getItem("DAYS_EVENTS"));
        // Verify that a local storage exists
        if (localStore != undefined) {
            Object.keys(localStore).forEach(key=>{
                const day = localStore[key];
                this.localStorageArray.push(day);
            });
        }
        
        // Find out if there are already events for this day
        let findIndexDay = this.localStorageArray.findIndex((day)=>{
            if (dayTemp.id == day.id) {
                return day;
            }
        });

        // Set events on the local storage
        if (this.localStorageArray[findIndexDay] != undefined) {
            dayTemp.events.shift();
            for (let index = 0; index < this.localStorageArray[findIndexDay].events.length; index++) {
                dayTemp.events.push(this.localStorageArray[findIndexDay].events[index]);
            }
        }
    }

    // Method for EventEmitter in DayComponent
    receiveDataFromDayComponent(day: DayModel){
        this.modalListComponent.selectedDay(day);
    }

    // receiveDataFromModalListComponent(event: EventModel) {
    //     this.dayComponent.deleteEventFromDay(event);
    // }




    // ########     ########   #######  ##     ## 
    // ##           ##     ## ##     ## ###   ### 
    // ##           ##     ## ##     ## #### #### 
    // ######       ##     ## ##     ## ## ### ## 
    // ##           ##     ## ##     ## ##     ## 
    // ##       ### ##     ## ##     ## ##     ## 
    // ##       ### ########   #######  ##     ## 
    // Steps to previous month
    previousMonth() {
        const currentDate = this.utilsService.changeMonth(this.year, this.month, -1);
        this.year = currentDate.year;
        this.month = currentDate.month;
        this.daysMonth = [];
        this.initTitle();
        this.initAfterView();
    };

    // Steps to next month
    nextMonth() {
        const currentDate = this.utilsService.changeMonth(this.year, this.month, 1);
        this.year = currentDate.year;
        this.month = currentDate.month;
        this.daysMonth = [];
        this.initTitle();
        this.initAfterView();
    };
}